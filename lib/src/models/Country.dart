class Country {
  final int id;
  final String countryName;
  final String countryFlagPath;

  int get Id => id;
  String get CountryName => countryName;
  String get CountryFlag => countryFlagPath;

  Country({this.id, this.countryName, this.countryFlagPath});

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
        id: json['id'],
        countryName: json['countryName'],
        countryFlagPath: json['countryFlagPath']);
  }
}
