import 'package:rxdart/rxdart.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/models/Country.dart';
import 'package:visabymasters/src/resources/repository.dart';

class CountryBloc {
  final Repository _repo = Repository();
  final PublishSubject<List<Country>> _countryListFetcher =
      PublishSubject<List<Country>>();

  Observable<List<Country>> get countryList => _countryListFetcher.stream;

  getCountries() async {
    Constants.loadingHeight = 0;
    List<Country> country = await _repo.getCountryList();
    _countryListFetcher.sink.add(country);
  }

  dispose() {
    _countryListFetcher.close();
  }
}

final CountryBloc bloc = CountryBloc();
