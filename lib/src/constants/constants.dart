class Constants {
  static double width;
  static double imageHeight = width / 3.5;
  static double height;
  bool isLoading = true;
  static double loadingHeight = 5;
  static String baseUrl = 'http://localhost:5000/api';
  static const String homeRoute = '/';
  static const String visaPage = '/visacountry';
  static const String applyNow = '/applynow';
}
