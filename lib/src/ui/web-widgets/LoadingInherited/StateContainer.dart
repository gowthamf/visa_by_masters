import 'package:flutter_web/material.dart';

class LoadingStateContainer extends StatefulWidget {
  final Widget child;
  final bool loading;

  LoadingStateContainer({@required this.child, this.loading});

  static _LoadingStateContainer of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(InheritedStateContainer)
            as InheritedStateContainer)
        .data;
  }

  void afterLoading(BuildContext context) {
    print('Built is done');
  }

  @override
  State<StatefulWidget> createState() {
    return _LoadingStateContainer();
  }
}

class _LoadingStateContainer extends State<LoadingStateContainer>
    with WidgetsBindingObserver {
  double loadingHeight = 5;

  void updateLoading(bool isLoading) {
    if (isLoading) {
      setState(() {
        // WidgetsBinding.instance.addPostFrameCallback(callback)
        loadingHeight = 5;
      });
    } else {
      setState(() {
        loadingHeight = 0;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    updateLoading(true);
  }

  @override
  Widget build(BuildContext context) {
    return InheritedStateContainer(
      data: this,
      child: widget.child,
    );
  }
}

class InheritedStateContainer extends InheritedWidget {
  final _LoadingStateContainer data;

  InheritedStateContainer(
      {Key key, @required this.data, @required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
