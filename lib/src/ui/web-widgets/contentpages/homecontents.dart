import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/ui/web-widgets/bestofus/performanceTemplate.dart';
import 'package:visabymasters/src/ui/web-widgets/checkvisa/checkvisa.dart';
import 'package:visabymasters/src/ui/web-widgets/contentpages/bottompanel.dart';
import 'package:visabymasters/src/ui/web-widgets/popular-visa/popularvisa.dart';
import 'package:visabymasters/src/ui/web-widgets/testimonials/testimonial.dart';
import 'package:visabymasters/src/ui/web-widgets/visa-types/visatypes.dart';
import 'package:visabymasters/utils/animationImage.dart';

class HomePageContents extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageContents();
  }
}

class _HomePageContents extends State<HomePageContents>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Constants.width = MediaQuery.of(context).size.width;
    Constants.height = MediaQuery.of(context).size.height;
    return Container(
      child: ListView(
        physics: const ClampingScrollPhysics(),
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: Constants.width,
                height: Constants.imageHeight,
                child: AnimationImage(),
              ),
              Card(
                child: CheckVisa(),
              ),
              Card(
                child: PopularVisaStateFul(),
              ),
              Card(
                child: VisaTypes(),
              ),
              Card(
                child: Testimonials(),
              ),
              Card(
                child: PerformanceTemplate(),
              ),
              Card(
                child: BottomPanel(),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
