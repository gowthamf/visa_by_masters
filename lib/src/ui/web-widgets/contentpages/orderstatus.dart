import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/blocs/country_bloc.dart';
import 'package:visabymasters/src/models/Country.dart';
import 'dart:async';
import 'package:flutter_web/services.dart';
import 'package:flutter_web/gestures.dart' show DragStartBehavior;

class OrderStatus extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _OrderStatus();
  }
}

class _OrderStatus extends State<OrderStatus> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white70,
        body: Center(
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Test",
                  style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
                ),
                SizedBox(
                  height: 50,
                ),
                TextField(
                  decoration: InputDecoration(
                      disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          borderSide: BorderSide(color: Colors.black)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          borderSide: BorderSide(color: Colors.black)),
                      labelText: "Test..."),
                )
              ],
            ),
          ),
        ));
  }
}
