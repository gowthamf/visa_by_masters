import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/ui/mobile-widgets/navigation/navigationbar.dart';
import 'package:visabymasters/src/ui/web-widgets/navigation/navigationbar.dart';
import 'package:visabymasters/utils/responsive_layout.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (ResponsiveLayout.isSmallScreen(context)) {
      return Container(
        child: Text('Mobile Version is under construction'),
      );
    }
    return Container(
      child: NavigationBar(),
    );
  }
}
