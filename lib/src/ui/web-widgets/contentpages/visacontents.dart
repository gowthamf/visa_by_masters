import 'dart:async';
import 'package:flutter_web/gestures.dart';
import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/ui/web-widgets/bestofus/performanceTemplate.dart';
import 'package:visabymasters/src/ui/web-widgets/contentpages/bottompanel.dart';
import 'package:visabymasters/src/ui/web-widgets/testimonials/testimonial.dart';
import 'package:visabymasters/src/ui/web-widgets/visa-types/visatypes.dart';

class VisaPage extends StatefulWidget {
  final Function changeTab;
  VisaPage({this.changeTab});
  @override
  State<StatefulWidget> createState() {
    return _VisaPage();
  }
}

class _VisaPage extends State<VisaPage> with TickerProviderStateMixin {
  bool visible = true;
  Timer timer;
  bool isShowing = true;

  @override
  void initState() {
    super.initState();
    _animated();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void _animated() {
    timer = Timer(Duration(milliseconds: 400), () {
      setState(() {
        visible = false;
      });
    });
  }

  void showVisa(PointerSignalEvent _) => setState(() => isShowing = false);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return AnimatedOpacity(
            opacity: visible ? 0.0 : 1.0,
            duration: const Duration(milliseconds: 500),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      'Visa by Masters- Visa Pre-check service documents will be reviewed within three hours of receipt and the contact for the order will receive a response to email requests received before 4:00 pm on Monday - Friday. Requests received after 4:00 pm will be responded to by 11:00 am the following business day.',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      'Note that file size sent can affect speed of transmission from your location.  Please reduce file size as much as possible before sending.',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      'Service Fees',
                      softWrap: true,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 25.0),
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      'Fees are exclusive of all other service fees, embassy fees or shipping fees. ',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      'Terms of Use',
                      softWrap: true,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 25.0),
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      'By submitting your documents to Visa by Masters for pre-checking you will be consenting to the following statements in addition to the standard Visa by Masters Terms of Use:',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      '•	To complete the Visa pre-check we will require a scan of the applicant’s identity page(s) of the passport along with any residence visas to be included with the supporting documentation for the visa.',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      '•	Visa by Masters will only check the documentation submitted together at the same time and it must include a Visa by Masters- Visa Pre-Check order form. You must submit all the required documents and forms that are listed in the Visa by Masters Visa Application kit or are listed on the country requirements.',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      '•	All visa processing fee is non-refundable and charges will apply at the time your documents are received and a check has been completed.',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      '•	We require users of Visa by Masters - Visa Pre-Check to submit approved paperwork within two business days (defer from country of visa requested). Orders submitted outside this time frame cannot be assumed to be correct due to changes that could be imposed by embassies.',
                      softWrap: true,
                    )),
                Container(
                    width: Constants.width,
                    padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                    child: Text(
                      'If you have questions contact a visa by masters specialist on 8888888888888888.',
                      softWrap: true,
                    )),
                Listener(
                    onPointerSignal: showVisa,
                    child: AnimatedOpacity(
                      opacity: isShowing ? 0.0 : 1.0,
                      duration: const Duration(milliseconds: 500),
                      child: Card(
                        child: VisaTypes(),
                      ),
                    )),
                Listener(
                  onPointerSignal: showVisa,
                  child: AnimatedOpacity(
                      opacity: isShowing ? 0.0 : 1.0,
                      duration: const Duration(milliseconds: 500),
                      child: Card(
                        child: Testimonials(),
                      )),
                ),
                Listener(
                  onPointerSignal: showVisa,
                  child: AnimatedOpacity(
                    opacity: isShowing ? 0.0 : 1.0,
                    duration: const Duration(milliseconds: 500),
                    child: Card(
                      child: PerformanceTemplate(),
                    ),
                  ),
                ),
                Card(
                  child: BottomPanel(),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
