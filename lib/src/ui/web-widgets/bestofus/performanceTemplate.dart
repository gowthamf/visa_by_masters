import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/ui/web-widgets/bestofus/customersupport.dart';
import 'package:visabymasters/src/ui/web-widgets/bestofus/rejection.dart';
import 'package:visabymasters/src/ui/web-widgets/bestofus/security.dart';
import 'package:visabymasters/src/ui/web-widgets/bestofus/speed.dart';

class PerformanceTemplate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: Constants.width,
        height: 500,
        child: Stack(
          children: <Widget>[
            Opacity(
              opacity: 0.65,
              child: Image.asset('images/travelimagetransparent.png'),
            ),
            Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Why Choose Us',
                    style: TextStyle(fontSize: 35),
                  ),
                ],
              ),
              Container(
                  width: 150,
                  margin: EdgeInsets.all(12.5),
                  child: Divider(
                    color: Colors.black,
                    height: 15.0,
                  )),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  padding: EdgeInsets.only(left: 100, right: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Card(
                        color: Colors.white70,
                        margin: EdgeInsets.only(top: 50, right: 50),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[Speed()],
                        ),
                      ),
                      Card(
                        color: Colors.white70,
                        margin: EdgeInsets.only(top: 50, right: 50),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[Security()],
                        ),
                      ),
                      Card(
                        color: Colors.white70,
                        margin: EdgeInsets.only(top: 50, right: 50),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[Rejections()],
                        ),
                      ),
                      Card(
                        color: Colors.white70,
                        margin: EdgeInsets.only(top: 50, right: 50),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[CustomerSupport()],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ])
          ],
        ));
  }
}
