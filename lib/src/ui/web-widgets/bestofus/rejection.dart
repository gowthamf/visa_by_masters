import 'package:flutter_web/material.dart';

class Rejections extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      margin: EdgeInsets.only(top: 50),
      height: 300,
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(bottom: 25),
                  child: Icon(
                    Icons.assignment_turned_in,
                    size: 75,
                  )),
              Text(
                'MINIMUM CHANCE OF REJECTION',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  'All documents are reviewed by a team of immigration experts before submission to respective governments. Our staff is well-trained and offers years of experience.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
