import 'package:flutter_web/material.dart';

class CustomerSupport extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      margin: EdgeInsets.only(top: 50),
      height: 300,
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(bottom: 25),
                  child: Icon(
                    Icons.insert_emoticon,
                    size: 75,
                  )),
              Text(
                'AWESOME CUSTOMER SUPPORT!',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  'Our call centers are ready to help you 24/7. We want you to enjoy your travels and ditch the stress of getting a visa!',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
