import 'package:flutter_web/material.dart';

class VisaCountry extends StatelessWidget {
  final dynamic countryId;

  VisaCountry(this.countryId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
        child: Text('$countryId'),
      ),
    );
  }
}
