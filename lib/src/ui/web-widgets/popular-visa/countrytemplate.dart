import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/ui/web-widgets/visabycountries/countryvisa.dart';

class CountryTemplate extends StatelessWidget {
  final List<Map<String, String>> countries;

  CountryTemplate(this.countries);

  Widget _createCountries(BuildContext context, int index) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Padding(
        padding: EdgeInsets.all(30.0),
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Image(
              fit: BoxFit.cover,
              image: AssetImage(countries[index]['image']),
            ),
          ),
          Text(
            countries[index]['countryname'],
            style: TextStyle(fontSize: 25),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Text('View More'),
                onPressed: () {
                  Navigator.pushNamed(context, Constants.visaPage,
                      arguments: countries[index]['id']);
                },
              )
            ],
          ),
        ],
      ),
    ]);
  }

  Widget _createCountryList() {
    Widget countryCard;

    if (countries.isNotEmpty) {
      countryCard = ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: _createCountries,
        itemCount: countries.length,
      );
    } else {
      countryCard = Container();
    }

    return countryCard;
  }

  @override
  Widget build(BuildContext context) {
    return _createCountryList();
  }
}
