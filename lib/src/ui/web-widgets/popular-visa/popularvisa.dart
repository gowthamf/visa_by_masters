import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/ui/web-widgets/popular-visa/countrytemplate.dart';
import 'package:visabymasters/utils/expansionPanel/expansion_panel.dart';

class Item {
  Item({
    this.expandedValue,
    this.headerValue,
    this.isExpanded = false,
  });

  Column expandedValue;
  Column headerValue;
  bool isExpanded;
}

List<Item> generateItems(int numberOfItems, {BuildContext context}) {
  List<Map<String, String>> _countries = [];
  Map<String, String> start = {
    'image': 'images/countries/aus.png',
    'countryname': 'Australia',
    'countrycode': '1'
  };
  Map<String, String> start1 = {
    'image': 'images/countries/br.png',
    'countryname': 'Brazil',
    'countrycode': '2'
  };
  Map<String, String> start2 = {
    'image': 'images/countries/jap.png',
    'countryname': 'Japan',
    'countrycode': '3'
  };
  Map<String, String> start3 = {
    'image': 'images/countries/nz.png',
    'countryname': 'New Zealand',
    'countrycode': '4'
  };
  Map<String, String> start4 = {
    'image': 'images/countries/uk.png',
    'countryname': 'United Kingdom',
    'countrycode': '5'
  };
  Map<String, String> start5 = {
    'image': 'images/countries/usa.png',
    'countryname': 'United States',
    'countrycode': '6'
  };

  _countries.add(start);
  _countries.add(start1);
  _countries.add(start2);
  _countries.add(start3);
  _countries.add(start4);
  _countries.add(start5);

  return List.generate(numberOfItems, (int index) {
    return Item(
      headerValue: Column(
        children: <Widget>[
          Text(
            'Most Popular Visas',
            style: TextStyle(fontSize: 35),
          ),
          Container(
              width: 150,
              margin: EdgeInsets.all(25.0),
              child: Divider(
                color: Colors.black,
                height: 10.0,
              )),
          SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Scrollbar(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                      child: CountryTemplate(_countries),
                    ),
                  ],
                ),
              ))
        ],
      ),
      expandedValue: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 1200,
                    height: 250,
                    child: CountryTemplate(_countries),
                  ),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 1200,
                    height: 250,
                    child: CountryTemplate(_countries),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  });
}

class PopularVisaStateFul extends StatefulWidget {
  PopularVisaStateFul({Key key}) : super(key: key);

  @override
  _PopularVisaState createState() => _PopularVisaState();
}

class _PopularVisaState extends State<PopularVisaStateFul> {
  List<Item> _data = generateItems(1);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _buildPanel(),
      ),
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelListC(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _data[index].isExpanded = !isExpanded;
        });
      },
      children: _data.map<ExpansionPanelC>((Item item) {
        return ExpansionPanelC(
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: item.headerValue,
            );
          },
          body: ListTile(
            title: item.expandedValue,
          ),
          isExpanded: item.isExpanded,
        );
      }).toList(),
    );
  }
}
