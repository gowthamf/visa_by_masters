import 'package:flutter_web/material.dart';

double _kMyLinearProgressIndicatorHeight = 5;

class CusLinearProgressIndicator extends LinearProgressIndicator
    implements PreferredSizeWidget {
  CusLinearProgressIndicator(
      {Key key,
      double value,
      Color backgroundColor,
      Animation<Color> valueColor,
      double heightProg})
      : super(
          key: key,
          value: value,
          backgroundColor: backgroundColor,
          valueColor: valueColor,
        ) {
    _kMyLinearProgressIndicatorHeight = heightProg;
    preferredSize = Size(double.infinity, _kMyLinearProgressIndicatorHeight);
  }

  @override
  Size preferredSize;
}
