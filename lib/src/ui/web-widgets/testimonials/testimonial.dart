import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/models/Country.dart';

class Testimonials extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Testimonials();
  }
}

class _Testimonials extends State<Testimonials> {
  PageController _pageController;
  Future<List<Country>> getCountry;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Constants.width,
      height: 500,
      child: Stack(
        children: <Widget>[
          Container(
            width: Constants.width,
            child: Opacity(
              opacity: 0.65,
              child: Image.asset(
                'images/testimonial-back.jpg',
                fit: BoxFit.fill,
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Testimonials',
                    style: TextStyle(fontSize: 35),
                  ),
                ],
              ),
              Container(
                  width: 500,
                  margin: EdgeInsets.all(25.0),
                  child: Text(
                    'Our customers have great stories about us',
                    style: TextStyle(fontSize: 25),
                    textAlign: TextAlign.center,
                  )),
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Text('data1'),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 250),
                      ),
                      Container(
                          width: Constants.width - 300,
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(Icons.arrow_left),
                                    onPressed: () {
                                      _pageController.previousPage(
                                        duration:
                                            const Duration(milliseconds: 400),
                                        curve: Curves.easeInOut,
                                      );
                                    },
                                  ),
                                  Container(
                                    width: Constants.width - 400,
                                    height: 300,
                                    child: PageView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: 2,
                                      itemBuilder: (context, position) {
                                        return Container(
                                            child: Row(
                                          children: <Widget>[
                                            Container(
                                              width: Constants.width - 400,
                                              color: position % 2 == 0
                                                  ? Colors.blue
                                                  : Colors.pink,
                                            ),
                                          ],
                                        ));
                                      },
                                      controller: _pageController,
                                    ),
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.arrow_right),
                                    onPressed: () {
                                      _pageController.nextPage(
                                        duration:
                                            const Duration(milliseconds: 400),
                                        curve: Curves.easeInOut,
                                      );
                                    },
                                  ),
                                ],
                              )
                            ],
                          )),
                    ],
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
