import 'package:flutter_web/material.dart';

class ApplicationForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ApplicationForm();
  }
}

class _ApplicationForm extends State<ApplicationForm> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Application Form'),
        ),
        body: Container(
            margin: EdgeInsets.only(top: 100),
            child: Column(
              children: <Widget>[
                Center(
                  widthFactor: 0.5,
                  child: TextFormField(
                    textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      filled: true,
                      icon: Icon(Icons.person),
                      hintText: 'What do people call you?',
                      labelText: 'Name',
                    ),
                  ),
                ),
                Center(
                  child: TextFormField(
                    textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      filled: true,
                      icon: Icon(Icons.person),
                      hintText: 'What do people call you?',
                      labelText: 'Name',
                    ),
                  ),
                )
              ],
            )));
  }
}
