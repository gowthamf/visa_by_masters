import 'package:http/http.dart' show Client;
import 'package:visabymasters/src/constants/constants.dart';
import 'dart:async';
import 'dart:convert';
import 'package:visabymasters/src/models/Country.dart';

class CountryApiProvider {
  Client client = Client();

  Future<List<Country>> getCountries() async {
    final response =
        await client.get(Constants.baseUrl + '/Country/GetCountires');

    List<Country> _countryList = [];

    if (response.statusCode == 200) {
      List<dynamic> countryList = json.decode(response.body);
      for (var i = 0; i < countryList.length; i++) {
        _countryList.add(Country.fromJson(countryList[i]));
      }
      return _countryList;
    } else {
      throw Exception('Failed to load Country List');
    }
  }
}
