import 'dart:async';

import 'package:visabymasters/src/models/Country.dart';
import 'package:visabymasters/src/resources/country_api_provider.dart';

class Repository {
  final CountryApiProvider countryApiProvider = CountryApiProvider();

  Future<List<Country>> getCountryList() => countryApiProvider.getCountries();
}
