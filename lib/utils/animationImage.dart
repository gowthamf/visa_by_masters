import 'dart:html';
import 'package:flutter_web_ui/ui.dart' as ui;
import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';

class AnimationImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ui.platformViewRegistry.registerViewFactory(
        'animation-Image-html',
        (int viewId) => ImageElement(
            src: 'assets/images/tourism.gif',
            width: (Constants.width).toInt(),
            height: (Constants.imageHeight).toInt()));
    return SizedBox(
      child: HtmlElementView(
        viewType: 'animation-Image-html',
      ),
    );
  }
}
