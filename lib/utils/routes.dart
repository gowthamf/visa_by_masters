import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/ui/web-widgets/applicationform/applicationform.dart';
import 'package:visabymasters/src/ui/web-widgets/contentpages/mainpage.dart';
import 'package:visabymasters/src/ui/web-widgets/visabycountries/countryvisa.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Constants.homeRoute:
        return MaterialPageRoute(builder: (context) => MainPage());
      case Constants.visaPage:
        var countryData = settings.arguments as dynamic;
        return MaterialPageRoute(
            builder: (context) => VisaCountry(countryData));
      case Constants.applyNow:
        return MaterialPageRoute(builder: (context) => ApplicationForm());
      default:
        return MaterialPageRoute(
            builder: (context) => Scaffold(
                  body: Center(
                    child: Text('No Route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
