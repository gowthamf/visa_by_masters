import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/ui/web-widgets/visabycountries/countryvisa.dart';

class DropDownList extends StatefulWidget {
  final Map<dynamic, String> dynamicDropDownList;
  final bool isCountryTo;
  final String hintText;
  final Color textColor;
  final Color dropDownTextColor;

  DropDownList(this.dynamicDropDownList, this.hintText, this.textColor,
      this.dropDownTextColor,
      {this.isCountryTo});
  @override
  State<StatefulWidget> createState() {
    return _DropDownList();
  }
}

class _DropDownList extends State<DropDownList> {
  dynamic selectedUser;
  Map<dynamic, String> dynamicDropDownList1;

  @override
  void initState() {
    super.initState();
    dynamicDropDownList1 = widget.dynamicDropDownList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.transparent,
        child: DropdownButton<dynamic>(
            disabledHint: Text('No Country Available'),
            iconSize: 30.0,
            style: TextStyle(
              color: Colors.black,
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
            ),
            hint: Text(
              widget.hintText,
              style: TextStyle(
                color: Colors.black,
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            value: selectedUser,
            onChanged: (dynamic newValue) {
              setState(() {
                selectedUser = newValue;
                if (widget.isCountryTo) {
                  Navigator.pushNamed(context, Constants.visaPage,
                      arguments: selectedUser);
                }
              });
            },
            items: dynamicDropDownList1.entries
                .map<DropdownMenuItem<String>>((MapEntry<dynamic, String> e) {
              return DropdownMenuItem<String>(
                value: e.key,
                child: Container(
                  child: Text(
                    e.value,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  width: 200,
                ),
              );
            }).toList()));
  }
}
