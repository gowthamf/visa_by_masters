import 'package:flutter_web/material.dart';
import 'package:visabymasters/src/constants/constants.dart';
import 'package:visabymasters/src/ui/web-widgets/LoadingInherited/StateContainer.dart';
import 'package:visabymasters/src/ui/web-widgets/contentpages/mainpage.dart';
import 'package:visabymasters/utils/routes.dart';

void main() {
  runApp(LoadingStateContainer(
    child: VisaByMastersWebApp(),
  ));
}

class VisaByMastersWebApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        onGenerateRoute: Router.generateRoute,
        initialRoute: Constants.homeRoute,
        title: 'Visa by Masters',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MainPage());
  }
}
